﻿using Newtonsoft.Json;
using QrCodeWithParameter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Services
{
    public class ApiService
    {
        public async Task<JSONResponse> CreateFolder(FolderDetails folderDetails)
        {
            var response = new JSONResponse();
            var request = new HttpClient();
            request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var url = "https://localhost:5022/api/folderdetails";
            var json = JsonConvert.SerializeObject(folderDetails);
            var jsonContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response1 = await request.PostAsync(url, jsonContent);

            if (response1.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response1.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<JSONResponse>(result);
            }
            else
            {
                return null;
            }

        }
        public async Task<List<FolderDetails>> GetFolders()
        {

            var request = new HttpClient();
            request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var url = "https://localhost:5022/api/folderdetails"; 

            var response = await request.GetAsync(url);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<FolderDetails>>(result);
            }
            else
            {
                return null;
            }

        }
    }
}
