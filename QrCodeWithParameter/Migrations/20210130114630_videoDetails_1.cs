﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QrCodeWithParameter.Migrations
{
    public partial class videoDetails_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VideoDetails",
                columns: table => new
                {
                    videoID = table.Column<string>(maxLength: 36, nullable: false),
                    videoTitle = table.Column<string>(nullable: true),
                    videoURL = table.Column<string>(nullable: true),
                    folderID = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoDetails", x => x.videoID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VideoDetails");
        }
    }
}
