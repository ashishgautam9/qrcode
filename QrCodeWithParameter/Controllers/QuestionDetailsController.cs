﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models;
using QrCodeWithParameter.Models.UserDetails;
using QrCodeWithParameter.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace QrCodeWithParameter.Controllers
{
    [Authorize]
    public class QuestionDetailsController : Controller
    {
        private readonly QrCodeWithParameterDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public QuestionDetailsController(QrCodeWithParameterDBContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

       
        [AllowAnonymous]
        // GET: QuestionDetails
        public async Task<IActionResult> Index()
        {
            //var applicationUser = new ApplicationUser();
            var currentUser = this.userManager.GetUserId(User);
            var qrContext = _context.QuestionDetails.Include(e => e.ApplicationUser).Where(e=>e.CreatorId == currentUser);
            return View(await _context.QuestionDetails.ToListAsync());
        }

       
        // GET: QuestionDetails/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionDetails = await _context.QuestionDetails
                .FirstOrDefaultAsync(m => m.QuestionId == id);           

            var currentUserId = userManager.GetUserId(User);
            var videoDetails = new VideoDetails();
            var videoId = await _context.AnswerDetails.Where(a => a.UserId == currentUserId).Select(a => a.VideoId).FirstOrDefaultAsync();
            var userName = User.Identity.Name;

            var applicationUsers = new ApplicationUser();

            List<string> videoIds = new List<string>();
            if (User.IsInRole("Admin"))
            {
               videoIds = await _context.AnswerDetails.Where(a => a.ParentFolderId == questionDetails.folderId).Select(a => a.VideoId).ToListAsync();
            }
            else
            {
                videoIds = await _context.AnswerDetails.Where(a => a.UserId == currentUserId).Where(a=>a.ParentFolderId == questionDetails.folderId).Select(a => a.VideoId).ToListAsync();
            }       


            ViewBag.VideoId = videoIds;
            //ViewBag.UserName = userName;

            //var answerDetails = new AnswerDetails();
           // var userId = await _context.AnswerDetails.Where(x => x.ParentFolderId == questionDetails.folderId).Select(x => x.UserId).ForEachAsync();
            //var userName = await 

            if (questionDetails == null)
            {
                return NotFound();
            }

            return View(questionDetails);
        }

        // GET: QuestionDetails/Create
        public IActionResult Create()
        {
            var questionDetails = new QuestionDetails();
            //questionDetails.CreatorId = this.userManager.GetUserId(HttpContext.User);
            questionDetails.CreatedBy = this.userManager.GetUserName(HttpContext.User);
            return View(questionDetails);
        }

        // POST: QuestionDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(QuestionDetails questionDetails,[FromForm] string description, [FromForm] string name)
        {
            var folderID = Guid.NewGuid().ToString();
            var folderDetails = new FolderDetails();
            folderDetails.Description = description;
            folderDetails.ID = folderID;
            folderDetails.isActive = true;
            folderDetails.Name = name;

            var applicationUser = new ApplicationUser();
            

            questionDetails.QuestionId = Guid.NewGuid().ToString();
            questionDetails.folderId = folderID;
            //var creatorId = userManager.Users.Where(a=> a.Email == "gautamash9@gmail.com").Select(a => a.Id);
            questionDetails.CreatorId = this.userManager.GetUserId(User);

            //questionDetails.CreatorId = this.userManager.GetUserId(HttpContext.User);
            //var creatorId = await _context.;
            /**var creatorID = userManager.Users.Select(a=>a.Id)
                .Where(Equals("GAUTAMASH9@GMAIL.COM"))
                .ToList();**/

            /**var creatorID = userManager.Users.
            .FromSqlRaw(@"SELECT id FROM applicationuser WHERE Name = {0}", "John Doe")
            .FirstOrDefault();**/

           // var currentUser = this.userManager.GetUserId(User);
            questionDetails.CreatedBy = User.Identity.Name;
            if (ModelState.IsValid)
            {
                _context.Add(questionDetails);
                await _context.SaveChangesAsync();

                ApiService apiService = new ApiService();
                var response =  apiService.CreateFolder(folderDetails);

                return RedirectToAction(nameof(Index));
            }
            return View(questionDetails);
        }

        // GET: QuestionDetails/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionDetails = await _context.QuestionDetails.FindAsync(id);
            if (questionDetails == null)
            {
                return NotFound();
            }
            return View(questionDetails);
        }

        // POST: QuestionDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("QuestionId,QuestionTitle,folderId,CreatedBy,CreatorId")] QuestionDetails questionDetails)
        {
            if (id != questionDetails.QuestionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(questionDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuestionDetailsExists(questionDetails.QuestionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(questionDetails);
        }

        // GET: QuestionDetails/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionDetails = await _context.QuestionDetails
                .FirstOrDefaultAsync(m => m.QuestionId == id);
            if (questionDetails == null)
            {
                return NotFound();
            }

            return View(questionDetails);
        }

        // POST: QuestionDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var questionDetails = await _context.QuestionDetails.FindAsync(id);
            _context.QuestionDetails.Remove(questionDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuestionDetailsExists(string id)
        {
            return _context.QuestionDetails.Any(e => e.QuestionId == id);
        }
    }
}
