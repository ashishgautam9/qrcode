﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models;

namespace QrCodeWithParameter.Controllers
{
    public class QrDetailsController : Controller
    {
        private readonly QrCodeWithParameterDBContext _context;

        public QrDetailsController(QrCodeWithParameterDBContext context)
        {
            _context = context;
        }

        // GET: QrDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.QrDetails.ToListAsync());
        }

        // GET: QrDetails/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var qrDetails = await _context.QrDetails
                .FirstOrDefaultAsync(m => m.ID == id);
            if (qrDetails == null)
            {
                return NotFound();
            }

            return View(qrDetails);
        }

        // GET: QrDetails/Create
        public IActionResult Create()
        {

            var model = new QrDetails();
            model.URL = "https://dev.boxklip.com/Account/QRLogin";
            model.ExpiryDate = DateTime.Now;
            model.UserID = "6bd9bd15-913e-4dbd-85dc-70bb5f1f7bcf";
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Validation(string id)
        {
            QrDetails qrDetails = new QrDetails();
            FileDetails fileDetails = new FileDetails();
            //string name = "ashishg3";
            //string userId = Request.QueryString["id"];
            //await _context.QrDetails.Where(x => x.UserID == name).FirstOrDefaultAsync();
            return RedirectToAction("Create", "FileDetails", new { url = id });
        }

        // POST: QrDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("URL,UserID,ExpiryDate,NoOfScans")] QrDetails qrDetails)
        {
            //string fileURL = Path.Combine(qrDetails.URL, qrDetails.UserID);
            string fileURL = qrDetails.URL + "?UserId=" + qrDetails.UserID;

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrData = qrGenerator.CreateQrCode(fileURL, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrData);
            Bitmap qrCodeImage = qrCode.GetGraphic(15);
            qrDetails.ID = Guid.NewGuid().ToString();
            qrDetails.QrByte = BitmapToBytes(qrCodeImage);
            qrDetails.CreatedDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Add(qrDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(qrDetails);
        }

        private static Byte[] BitmapToBytes(Bitmap qrCodeImage)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                qrCodeImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        // GET: QrDetails/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var qrDetails = await _context.QrDetails.FindAsync(id);
            if (qrDetails == null)
            {
                return NotFound();
            }
            return View(qrDetails);
        }

        // POST: QrDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ID,URL,UserID,CreatedDate,ExpiryDate,NoOfScans,QrByte")] QrDetails qrDetails)
        {
            if (id != qrDetails.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(qrDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QrDetailsExists(qrDetails.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(qrDetails);
        }

        // GET: QrDetails/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var qrDetails = await _context.QrDetails
                .FirstOrDefaultAsync(m => m.ID == id);
            if (qrDetails == null)
            {
                return NotFound();
            }

            return View(qrDetails);
        }

        // POST: QrDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var qrDetails = await _context.QrDetails.FindAsync(id);
            _context.QrDetails.Remove(qrDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QrDetailsExists(string id)
        {
            return _context.QrDetails.Any(e => e.ID == id);
        }
    }
}
