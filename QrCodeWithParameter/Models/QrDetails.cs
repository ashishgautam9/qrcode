﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class QrDetails
    {
        [StringLength(36)]
        public string ID { get; set; }

        public string URL { get; set; }

        public string UserID { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int NoOfScans { get; set; }

        public byte[] QrByte { get; set; }
    }
}
