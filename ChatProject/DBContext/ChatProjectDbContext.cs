﻿using ChatProject.Models;
using ChatProject.Models.UserDetails;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatProject.DBContext
{
    public class ChatProjectDbContext : IdentityDbContext<ApplicationUser, ApplicationRole,string>
    {
        public ChatProjectDbContext(DbContextOptions<ChatProjectDbContext> options) : base(options)
        {
        }

        public DbSet<Message> Message { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            builder.Entity<ApplicationRole>().ToTable("ApplicationRole");

            builder.Entity<ApplicationUser>().Property(h => h.Id).HasMaxLength(36).IsRequired();
            builder.Entity<ApplicationRole>().Property(h => h.Id).HasMaxLength(36).IsRequired();
        }
    }
}
