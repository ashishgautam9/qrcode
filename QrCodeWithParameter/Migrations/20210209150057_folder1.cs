﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QrCodeWithParameter.Migrations
{
    public partial class folder1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "createBy",
                table: "FolderDetails");

            migrationBuilder.AddColumn<string>(
                name: "createdBy",
                table: "FolderDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "createdBy",
                table: "FolderDetails");

            migrationBuilder.AddColumn<string>(
                name: "createBy",
                table: "FolderDetails",
                type: "text",
                nullable: true);
        }
    }
}
