﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QrCodeWithParameter.Models;
using QrCodeWithParameter.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace QrCodeWithParameter.DBContext
{
    public class QrCodeWithParameterDBContext : IdentityDbContext<ApplicationUser,ApplicationRole,string>
    {
        public QrCodeWithParameterDBContext(DbContextOptions<QrCodeWithParameterDBContext> options) : base(options)
        {
        }

        public DbSet<FileDetails> FileDetails { get; set; }

        public DbSet<QrDetails> QrDetails { get; set; }

        public DbSet<VideoDetails> VideoDetails { get; set; }

        public DbSet<FolderDetails> FolderDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            builder.Entity<ApplicationRole>().ToTable("ApplicationRole");

            builder.Entity<ApplicationUser>().Property(h => h.Id).HasMaxLength(36).IsRequired();
            builder.Entity<ApplicationRole>().Property(h => h.Id).HasMaxLength(36).IsRequired();
        }

        public DbSet<QrCodeWithParameter.Models.QuestionDetails> QuestionDetails { get; set; }

        public DbSet<AnswerDetails> AnswerDetails { get; set; }

        
    }
}
