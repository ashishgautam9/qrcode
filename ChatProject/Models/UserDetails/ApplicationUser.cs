﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChatProject.Models.UserDetails
{
    public class ApplicationUser : IdentityUser<string>
    {
        [StringLength(350)]
        public string FullName { get; set; }

        [StringLength(150)]
        public string Contact { get; set; }

        public bool IsPasswordUpdated { get; set; }
    }
}
