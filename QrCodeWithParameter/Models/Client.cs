﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class Client
    {
        [StringLength(36)]
        public string ID { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(200)]
        public string FullName { get; set; }

        [StringLength(15)]
        public string ContactNumber { get; set; }

    }
}
