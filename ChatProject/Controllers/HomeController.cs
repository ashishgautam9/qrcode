﻿using ChatProject.DBContext;
using ChatProject.Models;
using ChatProject.Models.UserDetails;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ChatProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ChatProjectDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public HomeController(ILogger<HomeController> logger, ChatProjectDbContext _context, UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            this._context = _context;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            //var msg = new Message();
            //msg.msgList = await _context.Message.ToListAsync();
            //var chatAppDBContext = _context.Message;            
            List<Message> msgList = new List<Message>();
            msgList = await _context.Message.ToListAsync();
            ViewBag.myList = msgList;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
