﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class FolderDetails
    {
        [StringLength(36)]
        public string ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool isActive { get; set; }

        public string createdBy { get; set; }

        public DateTime createdOn { get; set; }

        public FolderDetails()
        {
            this.createdOn = DateTime.Now;
        }
    }

}
