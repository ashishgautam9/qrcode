﻿using ChatProject.DBContext;
using ChatProject.Models;
using ChatProject.Models.UserDetails;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace ChatProject.Hubs
{    
    public class ChatHub : Hub
    {
        private readonly ChatProjectDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public ChatHub(ChatProjectDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        public async Task SendMessage(string user, string message)
        {            
            var messages = new Message();
            var userName = cp.Identity.Name;
            messages.Text = message;
            messages.UserName = user;
            await _context.Message.AddAsync(messages);
            await _context.SaveChangesAsync();
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
