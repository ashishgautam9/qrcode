﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QrCodeWithParameter.Migrations
{
    public partial class answerdetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AnswerDetails",
                columns: table => new
                {
                    AnswerID = table.Column<string>(maxLength: 36, nullable: false),
                    AnswerValue = table.Column<string>(maxLength: 500, nullable: true),
                    FolderId = table.Column<string>(maxLength: 36, nullable: true),
                    ParentFolderId = table.Column<string>(maxLength: 36, nullable: true),
                    VideoId = table.Column<string>(maxLength: 36, nullable: true),
                    UserId = table.Column<string>(maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnswerDetails", x => x.AnswerID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnswerDetails");
        }
    }
}
