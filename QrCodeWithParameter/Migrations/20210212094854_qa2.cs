﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QrCodeWithParameter.Migrations
{
    public partial class qa2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "QuestionDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuestionDetails_ApplicationUserId",
                table: "QuestionDetails",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuestionDetails_ApplicationUser_ApplicationUserId",
                table: "QuestionDetails",
                column: "ApplicationUserId",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuestionDetails_ApplicationUser_ApplicationUserId",
                table: "QuestionDetails");

            migrationBuilder.DropIndex(
                name: "IX_QuestionDetails_ApplicationUserId",
                table: "QuestionDetails");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "QuestionDetails");
        }
    }
}
