﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models;
using QrCodeWithParameter.Models.UserDetails;
using QrCodeWithParameter.Services;

namespace QrCodeWithParameter.Controllers
{
    public class VideoDetailsController : Controller
    {
        private readonly QrCodeWithParameterDBContext _context;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly UserManager<ApplicationUser> userManager;

        public VideoDetailsController(QrCodeWithParameterDBContext context,IWebHostEnvironment hostingEnvironment,UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.hostingEnvironment = hostingEnvironment;
            this.userManager = userManager;
        }

        // GET: VideoDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.VideoDetails.ToListAsync());
        }

        // GET: VideoDetails/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var videoDetails = await _context.VideoDetails
                .FirstOrDefaultAsync(m => m.videoID == id);
            if (videoDetails == null)
            {
                return NotFound();
            }

            return View(videoDetails);
        }

        [HttpGet]
        // GET: VideoDetails/Create
        public IActionResult Create(string id)
        {
            /**var folderList = (from folder in _context.FolderDetails
                              select new SelectListItem()
                              {
                                  Text = folder.Name,
                                  Value = folder.ID.ToString(),
                              }).ToList();
            folderList.Insert(0, new SelectListItem()
            {
                Text = "------Select--------",
                Value = string.Empty
            });

            VideoDetails videoDetails = new VideoDetails();
            videoDetails.listOfFolders = folderList;**/

            //var answerDetails = new AnswerDetails();
            //answerDetails.ParentFolderId = id;

            return View();
        }

        // POST: VideoDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VideoDetails videoDetails,[FromForm] string answer, string id)
        {
            var answerDetails = new AnswerDetails();
            var folderDetails = new FolderDetails();
            var questionDetail = new QuestionDetails();

            var questionId = await _context.QuestionDetails.Where(x => x.folderId == id).Select(x => x.QuestionId).FirstOrDefaultAsync();
            var folderID = Guid.NewGuid().ToString();
            var videoID = Guid.NewGuid().ToString();

            folderDetails.ID = folderID;
            folderDetails.Description = "";
            folderDetails.createdBy = User.Identity.Name;
            folderDetails.isActive = true;
            folderDetails.Name = User.Identity.Name;

            ApiService apiService = new ApiService();
            var response = apiService.CreateFolder(folderDetails);

            answerDetails.AnswerID = Guid.NewGuid().ToString();
            answerDetails.FolderId = folderID;
            //var userId = userManager.Users.Where(a => a.Email == "test@test.com").Select(a => a.Id);
            answerDetails.UserId = this.userManager.GetUserId(User);
            answerDetails.ParentFolderId = id;
            answerDetails.AnswerValue = answer;
            answerDetails.VideoId = videoID;

            videoDetails.videoID = videoID;
            
            string fileName = videoDetails.detailsVideo.FileName.ToString();
            string basePath = Path.Combine(hostingEnvironment.WebRootPath, "files\\");
            bool pathExists = System.IO.Directory.Exists(basePath);
            if (!pathExists)
            {
                Directory.CreateDirectory(basePath);
            }
            videoDetails.folderID = videoDetails.folderID;
            var filePath = Path.Combine(basePath, videoDetails.videoID+".mp4");
            videoDetails.videoURL= Path.Combine(filePath);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await videoDetails.detailsVideo.CopyToAsync(stream);
            }

            if (ModelState.IsValid)
            {
                _context.Add(videoDetails);
                await _context.SaveChangesAsync();

                _context.Add(answerDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "QuestionDetails",new { id = questionId});
            }
            return RedirectToAction("QuestionDetails","Index");
        }

        // GET: VideoDetails/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var videoDetails = await _context.VideoDetails.FindAsync(id);
            if (videoDetails == null)
            {
                return NotFound();
            }
            return View(videoDetails);
        }

        // POST: VideoDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("videoID,videoTitle,videoURL,folderID,CreatedDate")] VideoDetails videoDetails)
        {
            if (id != videoDetails.videoID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(videoDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VideoDetailsExists(videoDetails.videoID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(videoDetails);
        }

        // GET: VideoDetails/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var videoDetails = await _context.VideoDetails
                .FirstOrDefaultAsync(m => m.videoID == id);
            if (videoDetails == null)
            {
                return NotFound();
            }

            return View(videoDetails);
        }

        // POST: VideoDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var videoDetails = await _context.VideoDetails.FindAsync(id);
            _context.VideoDetails.Remove(videoDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VideoDetailsExists(string id)
        {
            return _context.VideoDetails.Any(e => e.videoID == id);
        }
    }
}
