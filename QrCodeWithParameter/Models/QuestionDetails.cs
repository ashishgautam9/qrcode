﻿using QrCodeWithParameter.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class QuestionDetails
    {
        [Key]
        [StringLength(36)]
        public string QuestionId { get; set; }

        [StringLength(350)]
        public string QuestionTitle { get; set; }

        [StringLength(36)]
        public string folderId { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(36)]
        public string CreatorId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
