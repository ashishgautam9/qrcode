﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QrCodeWithParameter.Models;
using QrCodeWithParameter.Services;

namespace QrCodeWithParameter.Controllers
{
    public class FolderController : Controller
    {
        // GET: Folder
        public async Task<IActionResult> Index()
        {
            ApiService api_services = new ApiService();
            var response = await api_services.GetFolders();
            return View(response);
        }

        // GET: Folder/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Folder/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Folder/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FolderDetails folderDetails, [FromForm] string question)
        {
            try
            {
                folderDetails.ID = Guid.NewGuid().ToString();
                folderDetails.isActive = true;
                folderDetails.createdBy = "kushal";

                ApiService api_services = new ApiService();
                var response = api_services.CreateFolder(folderDetails);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Folder/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Folder/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Folder/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Folder/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}