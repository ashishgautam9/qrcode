﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models;
using QrCodeWithParameter.Services;

namespace QrCodeWithParameter.Controllers
{
    public class FileDetailsController : Controller
    {
        private readonly QrCodeWithParameterDBContext _context;

        public FileDetailsController(QrCodeWithParameterDBContext context)
        {
            _context = context;
        }

        // GET: FileDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.FileDetails.ToListAsync());
        }

        // GET: FileDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileDetails = await _context.FileDetails
                .FirstOrDefaultAsync(m => m.FileID == id);
            if (fileDetails == null)
            {
                return NotFound();
            }

            return View(fileDetails);
        }

        // GET: FileDetails/Create
        public IActionResult Create(string url)
        {


            var fileDetails = new FileDetails();
            fileDetails.UserID = url;
            return View(fileDetails);
        }

        // POST: FileDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FileDetails fileDetails)
        {
            //string filePath = Path.Combine(hostingEnvironment.WebRootPath, fileDetails.UserID, "files");
            //fileDetails.FileID = Guid.NewGuid();
            //fileDetails.FileName = fileDetails.detailsFile.FileName.ToString();
            //fileDetails.FileLocation = Path.Combine(filePath, fileDetails.detailsFile.FileName);
            ////fileDetails.detailsFile.CopyTo(new FileStream(filePath, FileMode.Create));
            //fileDetails.FileExtension = fileDetails.detailsFile.ContentType;
            //if (ModelState.IsValid)
            //{
            //    _context.Add(fileDetails);
            //    await _context.SaveChangesAsync();
            //    return RedirectToAction(nameof(Index));
            //}
            return View(fileDetails);
        }

        // GET: FileDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileDetails = await _context.FileDetails.FindAsync(id);
            if (fileDetails == null)
            {
                return NotFound();
            }
            return View(fileDetails);
        }

        // POST: FileDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("FileID,UserID,FileName,FileLocation,FileExtension,CreatedTime")] FileDetails fileDetails)
        {
            if (id != fileDetails.FileID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fileDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FileDetailsExists(fileDetails.FileID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(fileDetails);
        }

        // GET: FileDetails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileDetails = await _context.FileDetails
                .FirstOrDefaultAsync(m => m.FileID == id);
            if (fileDetails == null)
            {
                return NotFound();
            }

            return View(fileDetails);
        }

        // POST: FileDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var fileDetails = await _context.FileDetails.FindAsync(id);
            _context.FileDetails.Remove(fileDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FileDetailsExists(Guid id)
        {
            return _context.FileDetails.Any(e => e.FileID == id);
        }
    }
}
