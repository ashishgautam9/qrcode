﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models;

namespace QRWithAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FolderDetailsController : ControllerBase
    {
        private readonly QrCodeWithParameterDBContext _context;

        public FolderDetailsController(QrCodeWithParameterDBContext context)
        {
            _context = context;
        }

        // GET: api/FolderDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FolderDetails>>> GetFolderDetails()
        {
            return await _context.FolderDetails.ToListAsync();
        }

        // GET: api/FolderDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FolderDetails>> GetFolderDetails(string id)
        {
            var folderDetails = await _context.FolderDetails.FindAsync(id);

            if (folderDetails == null)
            {
                return NotFound();
            }

            return folderDetails;
        }

        // PUT: api/FolderDetails/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFolderDetails(string id, FolderDetails folderDetails)
        {
            if (id != folderDetails.ID)
            {
                return BadRequest();
            }

            _context.Entry(folderDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FolderDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FolderDetails
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FolderDetails>> PostFolderDetails([FromBody]FolderDetails folderDetails)
        {
            Console.WriteLine(JsonConvert.SerializeObject(folderDetails).ToString());
            _context.FolderDetails.Add(folderDetails);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FolderDetailsExists(folderDetails.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFolderDetails", new { id = folderDetails.ID }, folderDetails);
        }

        // DELETE: api/FolderDetails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FolderDetails>> DeleteFolderDetails(string id)
        {
            var folderDetails = await _context.FolderDetails.FindAsync(id);
            if (folderDetails == null)
            {
                return NotFound();
            }

            _context.FolderDetails.Remove(folderDetails);
            await _context.SaveChangesAsync();

            return folderDetails;
        }

        private bool FolderDetailsExists(string id)
        {
            return _context.FolderDetails.Any(e => e.ID == id);
        }
    }
}
