﻿// <auto-generated />
using ChatProject.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ChatProject.Migrations
{
    [DbContext(typeof(ChatProjectDbContext))]
    [Migration("20210228152636_first")]
    partial class first
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.12")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("ChatProject.Models.Message", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Text")
                        .HasColumnType("varchar(300)")
                        .HasMaxLength(300);

                    b.Property<string>("UserName")
                        .HasColumnType("varchar(36)")
                        .HasMaxLength(36);

                    b.HasKey("ID");

                    b.ToTable("Message");
                });
#pragma warning restore 612, 618
        }
    }
}
