﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using QrCodeWithParameter.DBContext;
using QrCodeWithParameter.Models.UserDetails;
using QrCodeWithParameter.ViewModel;

namespace QrCodeWithParameter.Controllers
{
    public class AccountController : Controller
    {
        private readonly QrCodeWithParameterDBContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ILogger<AccountController> _logger;

        public AccountController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            QrCodeWithParameterDBContext context,
            ILogger<AccountController> _logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this._context = context;
            this._logger = _logger;
        }

        // GET: Account
        public async Task<IActionResult> Index()
        {
            return View(await _context.Users.ToListAsync());
        }

        public IActionResult Register(string NewId)
        {
            var model = new RegistrationViewModel();
            model.NewId = NewId;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegistrationViewModel model)
        {
            var UserType = string.Empty;
            if(ModelState.IsValid)
            {
                var user = new ApplicationUser() { Id = Guid.NewGuid().ToString(), UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);

                if(result.Succeeded)
                {
                    user.FullName = model.FullName;
                    user.Contact = model.Contact;
                    user.IsPasswordUpdated = User.Identity.IsAuthenticated == true ? true : false;

                    _context.Users.Update(user);
                    await _context.SaveChangesAsync();

                    if(!User.Identity.IsAuthenticated)
                    {
                        await userManager.AddToRoleAsync(user, "Admin");
                        await signInManager.SignInAsync(user, isPersistent: false);
                        return RedirectToAction("Login", "Account");
                    }
                    try
                    {
                        await userManager.AddToRoleAsync(user, "Client");
                        //   this.emailService.Mail_Alert(model.Email, "Successfully Registered", "<h1>Welcome to Boxklip</h1><p>You have been registered as a client.</p><p>Email:" + model.Email + "</p><p>Password:" + model.Password + "</p>");
                        return RedirectToAction("Login", "Account");
                    }
                    catch
                    {

                    }

                    return RedirectToAction("Client", "Account");
                }

                foreach(var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }                
            }
            return View(model);
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if(ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(loginViewModel.Email, loginViewModel.Password, loginViewModel.RememberMe, false);

                if(result.Succeeded)
                {
                    var userDetails = await _context.Users.Where(x => x.UserName == loginViewModel.Email).FirstOrDefaultAsync();
                    var userRole = await this.userManager.GetRolesAsync(userDetails);

                    if(HttpContext.User != null)
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Role,userRole.FirstOrDefault()),
                            new Claim(ClaimTypes.NameIdentifier, userDetails.Id.ToString()),
                        };

                        var appIdentity = new ClaimsIdentity(claims);
                        HttpContext.User.AddIdentity(appIdentity);
                    }

                    if(userRole.Contains("Admin"))
                    {
                        return RedirectToAction("Create", "QuestionDetails");
                    }
                    else
                    {                       
                        return RedirectToAction("Index", "QuestionDetails");                        
                    }
                }

                ModelState.AddModelError("", "Invalid Login Attempt");
            }

            return View(loginViewModel);
        }
        // GET: Account/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // GET: Account/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FullName,Contact,IsPasswordUpdated,Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(applicationUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: Account/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }
            return View(applicationUser);
        }

        // POST: Account/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("FullName,Contact,IsPasswordUpdated,Id,UserName,NormalizedUserName,Email,NormalizedEmail,EmailConfirmed,PasswordHash,SecurityStamp,ConcurrencyStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnd,LockoutEnabled,AccessFailedCount")] ApplicationUser applicationUser)
        {
            if (id != applicationUser.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicationUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationUserExists(applicationUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: Account/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: Account/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var applicationUser = await _context.Users.FindAsync(id);
            _context.Users.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationUserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
