﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class FileDetails
    {
        [Key]
        public Guid FileID { get; set; }

        [StringLength(200)]
        public string UserID { get; set; }


        [StringLength(200)]
        public string FileName { get; set; }

        [StringLength(400)]
        public string FileLocation { get; set; }


        [StringLength(20)]
        public string FileExtension { get; set; }

        public DateTime CreatedTime { get; set; }

        [NotMapped]
        public IFormFile detailsFile { get; set; }
    }
}
