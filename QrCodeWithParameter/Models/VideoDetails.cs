﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class VideoDetails
    {
        [Key]
        [StringLength(36)]
        public string videoID { get; set; }

        public string videoTitle { get; set; }

        public string videoURL{ get; set; }

        public string folderID { get; set; }

        public DateTime CreatedDate { get; set; }

        [NotMapped]
        public IFormFile detailsVideo { get; set; }

        [NotMapped]
        public List<SelectListItem> listOfFolders { get; set; }
    }
}
