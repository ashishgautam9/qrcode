﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QrCodeWithParameter.Models
{
    public class AnswerDetails
    {
        [Key]
        [StringLength(36)]
        public string AnswerID { get; set; }

        [StringLength(500)]
        public string  AnswerValue { get; set; }

        [StringLength(36)]
        public string FolderId { get; set; }

        [StringLength(36)]
        public string ParentFolderId { get; set; }

        [StringLength(36)]
        public string VideoId { get; set; }

        [StringLength(36)]
        public string UserId { get; set; }
    }

}
