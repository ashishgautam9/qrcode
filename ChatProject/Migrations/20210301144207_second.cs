﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatProject.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MessageID",
                table: "Message",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Message_MessageID",
                table: "Message",
                column: "MessageID");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_Message_MessageID",
                table: "Message",
                column: "MessageID",
                principalTable: "Message",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_Message_MessageID",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_MessageID",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "MessageID",
                table: "Message");
        }
    }
}
