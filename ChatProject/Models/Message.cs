﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChatProject.Models
{
    public class Message
    {
        public int ID { get; set; }

        [StringLength(36)]
        public string UserName { get; set; }

        [StringLength(300)]
        public string Text { get; set; }

        public List<Message> msgList { get; set; }
    }
}
